# sumTime Docs

### Autores:

+ Dione Silveira
+ Felipe Corrêa
+ Fernando Angelin
+ Ivan Rambo
+ Mateus Nascimento
+ Nicolas Bisi

## Como rodar
> Not yet...

## Git Basics - Como contribuir

### Configurando o repositório:
> Este trabalho será realizado utilizando o GIT _[1]_. Cheque as referências para um link explicando como instalar e configurar o GIT no seu ambiente. O **flow** de trabalho será **Fork->Pull Request->Master**, que será explicado passo a passo a seguir.

> Nosso repositório central, onde não deveremos fazer commits diretamente, será o: [https://bitbucket.org/angelin/dsdesktop](https://bitbucket.org/angelin/dsdesktop). Para começar, faça um **fork** do repositório central, atribuindo qualquer nome à ele. O Fork resultante será onde programaremos.

> Após fazer um **fork** do repositório central, clone este fork resultante cujo link deve ser algo semelhante à:
    
    http://bitbucket.org/<seu_nome_de_usuário/<nome_do_seu_fork>
    
> Após clonado para uma pasta no computador, navegue via terminal até a pasta do seu fork. Agora é preciso adicionar o repositório central (angelin/dsdesktop) como **REMOTE** do seu fork. O remote é uma referência ao repositório central para puxar atualizações e manter o fork em dia em relação ao central. 

    git remote add upstream  https://(usuario_bitbucket)@bitbucket.org/angelin/dsdesktop.git

> Note que deve ser alterado ** (usuario_bitbucket) ** pelo seu nome de usuário do Bitbucket. Agora, o repositório central está na lista de referências do seu fork.

> Sempre antes de puxar atualizações do repositório central, certifique-se de que está no branch **'master'**. Para mudar de branch, digite:

    git checkout master

> Para listar os branches disponíveis no seu local, digite:

    git branch
    
> Estando no master, para puxar as atualizações do repositório central, agora contido em upstream/master, basta digitar:

    git fetch upstream
    
> Estas mudanças serão baixadas para seu local, mas **não serão aplicadas**! Para aplicar uma mudança, é preciso fazer merge das modificações baixadas do repositório central com seu master. Basta digitar:

    git merge upstream/master

> Essas mudanças serão aplicadas ao seu fork local, mas ainda não estarão no server do bitbucket, afinal, não houve push. Para enviar para o servidor:

    git push origin master
    
> Com seu master atualizado, está garantido que será modificada uma versão atualizada do software. **Sempre tome cuidado em estar atualizado para modificar**, pois isso diminui o custo dos merges do repositório central. Agora, é só programar! Uhuul. :)

### Fluxo para programar

> Para programar, procure atualizar seu repositório com o master. Caso você venha do passo anterior, não precisa fazer essa parte de novo, seu imbecil.

    git checkout master
    git fetch upstream
    git merge upstream/master
    git push origin master
    
> Após certificar que está atualizado, mude para um branch de desenvolvimento, por exemplo:

    git branch devel

> Assim criado o branch, faça um checkout para ele:

    git checkout devel
    
> Para deletar um branch existente, caso não queiras mais ele (**não deletar o master... sério!**):

    git branch -D <nome_do_branch>
    
> Agora, pode-se programar... Após feitas as modificações no código, cheque a lista de arquivos alterados com este comando:

    git status

> Adicione apenas os arquivos RELEVANTES (aka, .java ou .class, ou mídia como imagens/audio/video) para a bandeja de commit. Faça isso utilizando:

    git add <nome_do_arquivo>
    
> **Não é recomendado o git add * ou git add -a, pois o Netbeans gera muito lixo! Facilite a vida de quem vai revisar o commit de vocês. **

> Assim que todos arquivos que deseja-se commitar forem adicionados, pode ser feito o commit. **Temos padrões para mensagens de commit, cheque na próxima seção do tutorial**. 

    git commit -m "[#Tipo de Modificação - Título]
    Descrição breve
    ---
    ---
    "

> Para enviar para o servidor:
    
    git push origin <nome_do_branch>
    
    no caso,
    git push origin devel

> Agora feito os commits, quando achar que o seu trabalho feito nesse branch que chamamos de devil está feito, você pode fazer um **pull request**. O pull request é um pedido de merge de um branch ao branch principal.

> Para fazer isso, utilizaremos a interface gráfica do bitbucket.org. Vá até o seu fork e clique em pull request, selecionando o branch origem e o branch destino, no caso, sempre o branch devil do repositório central.** Não faça pull request direto para o master, o responsável pelos merges do repositório central deverá fazer isso. **



> Preenchendo o nome e descrição, marque o checkbox "Close <branch> after merge". 

> Agora, basta esperar a revisão do seu pull request.

>



> É isso! Dúvidas, tratar com **Marcelo Fay**.


### Padrões de nomes de commit

> Para ficar organizado, usaremos um padrão nas mensagens de commit.

    [#Tipo de Commit - Nome]
    Descrição
    
#### Tipos de Commit:
####
+ New Feature 
+ Feature Update
+ Bug Fix
+ Merge
+ Design Update

> Não é preciso seguir tão a risca, mas facilita a leitura do histórico.


## Referências:
> [1] - GIT - How to Install - [http://git-scm.com/book/en/Getting-Started-Installing-Git](http://git-scm.com/book/en/Getting-Started-Installing-Git)
