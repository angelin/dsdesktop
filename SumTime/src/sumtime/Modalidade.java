/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author sumTime Group
 */
public class Modalidade {

    private String categoria;
    private float minimoHoras;
    
    /**
     * 
     * Construttor sem parametros
     */
    public Modalidade(){
        this.categoria = "";
        this.minimoHoras = 0;
    }
    
    /**
     * 
     * @param categoria
     * @param minimoHoras 
     */
    public Modalidade(String categoria, float minimoHoras){
        this.categoria = categoria;
        this.minimoHoras = minimoHoras;
    }
    
    /**
     * 
     * @param categoria 
     */
    public void setCategoria(String categoria){
        this.categoria = categoria;
    }
    
    /**
     * 
     * @return categoria
     */
    public String getCategoria(){
        return categoria;
    }
    
    /**
     * 
     * @param minimoHoras 
     */
    public void setMinimoHoras(float minimoHoras){
        this.minimoHoras = minimoHoras;
    }
    
    /**
     * 
     * @return minimoHoras
     */
    public float getMinimoHoras(){
        return minimoHoras;
    }
}
